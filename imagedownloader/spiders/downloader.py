# -*- coding: utf-8 -*-
import scrapy


class DownloaderSpider(scrapy.Spider):
    name = 'downloader'
    
    def __init__(self,*args,**kwargs):
    	super(DownloaderSpider,self).__init__(*args,**kwargs)
    	self.start_urls = ['https://www.pexels.com/search/'+kwargs.get('value')]


    def parse(self, response):
        list1=response.xpath("//img").extract()
        file=open("imagecatcher.html","w+")
        for i in list1:
        	file.write(f"<div>{i}</div>")
        file.close()
